#include <bits/stdc++.h>
#include<ext/pb_ds/assoc_container.hpp>
#include<ext/pb_ds/tree_policy.hpp>

using namespace std ;
using namespace __gnu_pbds;

template <typename T> // *s.find_by_order(0), s.order_of_key(2) ;
using ordered_set = tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;

#define reMin(a, b) a = min(a, b)
#define reMax(a, b) a = max(a, b)

#define lint long long
#define pb push_back
#define F first 
#define S second 
#define sz(x) (int)x.size()
#define all(x) begin(x), end(x)
#define SET(x, val) memset(x, val, sizeof(x))
#define fastio ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0)

typedef vector < int > vi ;
typedef pair < int, int > pii ;
typedef pair < double, double > point ;

const int N = 1e5 + 2 ;
const int M = 3000  + 2 ;
const lint INF = 1e18 ;
lint MAX_OPS = 1e8 ; 

const int TAGS = 501 ;
int n ;

// TYPE:
// 0 for vertical
// 1 for horizontal 
// tags and tags2 are same (only difference is one is set and another is vector)

struct photo {
    set < int > tags ; vi tags2 ; 
    int type ;
    int index ; 
    bitset < TAGS > occur ; 
};
vector < photo > photos ; 

struct slide {
    set < int > tags ; vi tags2 ; 
    vi photos ;  // contains index of photos in this slide 
    bitset < TAGS > occur ;
};
vector < slide > totalSlides ; 


void outputSol(vector < slide >& ans) {
    int cnt = sz(ans) ;
    cout << cnt << endl ; 

    for(int i = 0; i < cnt; i++) {
        for(int j = 0; j < sz(ans[i].photos); j++) {
            cout << ans[i].photos[j] ;
            if(j < (sz(ans[i].photos) - 1)) cout << " " ;
        }
        if(i < (cnt - 1)) cout << endl ; 
    }
}

bitset < TAGS > res ;
int score(slide& s1, slide& s2) {
    res = s1.occur & s2.occur ;
    int inter = res.count() ;

    res ^= s1.occur ; 
    int dif1 = res.count() ;

    res = s1.occur & s2.occur ;
    res ^= s2.occur ;
    int dif2 = res.count();

    return min(inter, min(dif1, dif2));
}

int getScore(vector < slide >& slides) {
    int s =0; int cnt = sz(slides);
    for(int i = 1; i < cnt; i++) {
        s += score(slides[i-1], slides[i]);
    }
    return s ; 
}
// stores indices of horizontal/vertical photos 
vi vertical, horizontal ; 
int taken[N];
map < string, int > tagIndex ; 
vector < slide > hor, ver ; 

void lsSlides(vector < slide > &slides) {
    int totalTags = 0 ;
    for(auto s : slides) totalTags += s.occur.count();
    int cur = totalTags ;
    
    int stuckIter = 0;

}

int checkSwap(vector < slide > &slides, int i, int j) {
    int cnt = sz(slides);

    int bef = 0 ;
    if(i) bef += score(slides[i],slides[i-1]);
    if(i < (cnt - 1)) bef += score(slides[i],slides[i+1]);

    if(j) bef += score(slides[j],slides[j-1]);
    if(j < (cnt - 1)) bef += score(slides[j],slides[j+1]);

    swap(slides[i], slides[j]);

    int aft = 0 ; 
    if(i) aft += score(slides[i],slides[i-1]);
    if(i < (cnt - 1)) aft += score(slides[i],slides[i+1]);

    if(j) aft += score(slides[j],slides[j-1]);
    if(j < (cnt - 1)) aft += score(slides[j],slides[j+1]);

    if(bef > aft) swap(slides[i], slides[j]);

    if(aft > bef) return aft - bef ;
    return 0;
}

void lsPermutation(vector < slide > &slides) {
    int curScore = getScore(slides);
    int bestScore = curScore ;
    vector < slide > bestPermutation;
    int cnt = sz(slides);

    int OPS = 90000 ;
    int iter = 0 ; int delta = 0 ;
    int stuckIter = 0 ;

    // vector < pii > pairs ;
    // for(int i = 0; i < cnt; i++) for(int j = i+1; j < cnt; j++) pairs.pb({i, j});
    // random_shuffle(all(pairs));

    int idx = 0 ; 

    while(iter < OPS) {

        int evals = 0; delta = 0 ;
        // while(evals < cnt) {
        //     int i = pairs[idx].F, j = pairs[idx].S ;
        //     idx++ ; idx %= cnt ;
        //     delta = checkSwap(slides, i, j);
        //     curScore += delta ;
        //     evals++ ;
        //     if(delta) break ;
        // }
        for(int i = 0; i < cnt; i++) {
            bool toBreak = 0 ; 
            for(int j = i+1; j < cnt; j++) {
                delta = checkSwap(slides, i, j);
                curScore += delta ;
                if(delta) {
                    toBreak = 1 ;
                    break ;
                }
            }
            if(toBreak) break ; 
        }

        if(!delta) stuckIter++ ;
        else stuckIter = 0 ;

        if(stuckIter) break ; 
        iter++ ;
        if(iter % 500 == 1) cerr << curScore << endl ; 
    }
}

int main(int argc, char* argv[])
{
    srand(time(0)); 
    freopen(argv[1], "r", stdin); 

    cin >> n; int tagCnt ;   
    photos.resize(n); char ch ; 
    set < int > total ; string dummy ; 
    vi distinctTags ;
    int tagIdx = 0 ;

    for(int i = 0; i < n; i++) {
        cin >> ch ; photos[i].type = (ch == 'H');
        photos[i].index = i ; 

        cin >> tagCnt ;
        photos[i].tags2.resize(tagCnt);

        for(int j = 0; j < tagCnt; j++) {
            cin >> dummy ;

            int idx ;
            if(tagIndex.find(dummy) == tagIndex.end()) tagIndex[dummy] = tagIdx++ ;
            idx = tagIndex[dummy] ;
            // cerr << idx << endl ;

            photos[i].tags.insert(idx);
            photos[i].tags2[j] = idx ; 
        }
        if(!photos[i].type) vertical.pb(i);
        else horizontal.pb(i);
    }

    // intialize bitsets
    for(int i = 0; i < n; i++) {
        for(auto j : photos[i].tags2)
            photos[i].occur.set(j);
    }

    cerr << "Making slides\n" ;

    SET(taken, 0); 
    // make slides greedily 
    int k = sz(vertical);
    for(int i = 0; i < k; i++) {
        int idx = vertical[i] ; 
        slide s ; 
        bitset < TAGS > occur ;
        int bestIdx = -1 ;
        bitset < TAGS > best ; 

        if(taken[idx]) continue ; 
        if(photos[idx].type) {
            s.occur = photos[idx].occur ;
            s.photos.pb(idx);
            hor.pb(s);
            continue ;
        }        

        for(int j = i+1; j < k; j++) {
            int idx2 = vertical[j] ; 
            if(photos[idx2].type or taken[idx2]) continue ;

            occur = photos[idx].occur ;
            occur |= photos[idx2].occur ;

            if(occur.count() > best.count()) {
                best = occur ;
                bestIdx = idx2 ;
            }
            // break ; 
        }
        s.occur = best ;
        s.photos.pb(idx); s.photos.pb(bestIdx) ; 
        ver.pb(s); 
        taken[bestIdx] = 1; taken[idx] = 1 ; 
        if(i % 1000 == 2) cerr << "Done for slide " << i << endl ; 
    }

    cerr << sz(photos) << endl ; 

    for(auto& s: ver) totalSlides.pb(s);
    for(auto& s: hor) totalSlides.pb(s);

    vector < slide > bestOrder ;
    random_shuffle(all(totalSlides));

    bestOrder.pb(totalSlides.back()); totalSlides.pop_back();
    while(!totalSlides.empty()) {
        slide& s = bestOrder[sz(bestOrder) - 1] ;
        int bestIndex = 0 ; int bestNeighbor = -1 ;

        for(int i = 0; i < sz(totalSlides); i++) {
            if(score(s, totalSlides[i]) > bestNeighbor) {
                bestNeighbor = score(s, totalSlides[i]);
                bestIndex = i ;
            }
        }
        bestOrder.pb(totalSlides[bestIndex]);
        swap(totalSlides[bestIndex], totalSlides[sz(totalSlides) - 1]);
        totalSlides.pop_back();
        if(sz(totalSlides) % 1000 == 23) cerr << "Left to do: " << sz(totalSlides) << endl ;
    }
    totalSlides = bestOrder ;

    int OPS = 100 ; int iter = 0 ;
    vector < slide > bestPerm ; 
    int bestScore = -1 ;

    while(iter < OPS)    {
        int curScore = getScore(totalSlides); cerr << curScore << endl ; 
        lsPermutation(totalSlides);
        curScore = getScore(totalSlides);

        if(curScore > bestScore) {
            bestScore = curScore ;
            bestPerm = totalSlides ;
            cerr << "best score till now: " << bestScore << endl ; 
        }
        random_shuffle(all(totalSlides));
        iter++ ; 
        break ;
    }

    freopen("c.out", "w", stdout);
    outputSol(bestPerm);
    return 0;   
}
